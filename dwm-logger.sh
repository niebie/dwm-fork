#! bash

set maybe_shell=replace_me
set shell=bash
if [$maybe_shell] == [replace_me]
   set shell=$maybe_shell
fi
$shell -c $1 2>&1 > /var/log/dwm.log
