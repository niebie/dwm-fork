/* See LICENSE file for copyright and license details. */

/* appearance */
static const char *fonts[] = {
  "monospace:size=10"
};
static const char dmenufont[]       = "monospace:size=10";
static const char normbordercolor[] = "#cc4400";
static const char normbgcolor[]     = "#cccccc"; /* "#110900"; */
static const char normfgcolor[]     = "#110900"; /* "#ffffff"; */ /* tag/name font */
static const char selbordercolor[]  = "#3465A4"; /* focused window border */
static const char selbgcolor[]      = "#3465A4"; /* top bar background */
static const char selfgcolor[]      = "#ffffff";
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
  /* xprop(1):
   *	WM_CLASS(STRING) = instance, class
   *	WM_NAME(STRING) = title
   */
  /* class      instance    title       tags mask     isfloating   monitor */
  { "Gimp",     NULL,       NULL,       0,            1,           -1 },
  { "Firefox",  NULL,       NULL,       1 << 7,       0,           -1 },
  { "google-chrome", NULL,  NULL,       1 << 8,       0,           -1 },
  { "LibreOffice", NULL,    NULL,       1 << 6,       0,           -1 },
  { "steam      ", NULL,    NULL,       1 << 3,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int nviews      = 1;    /* number of tags highlighted by default */
static const int resizehints = True;    /* 1 means respect size hints in tiled resizals */
static const Bool statusall  = True;	/* True means status is show in all bars, not just active monitor */

static const int master[1];	     /* nmaster override per monitor */
static const int views[1];

static const Layout layouts[] = {
  /* symbol     arrange function */
  { "[]=",      tile },    /* first entry is default */
  { "><>",      NULL },    /* no layout function means floating behavior */
  { "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define WINKEY Mod2Mask
#define TAGKEYS(KEY,TAG)												\
  { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
  { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
  { MODKEY|WINKEY,                KEY,      nview,          {.ui = 1 << TAG} }, \
  { MODKEY|WINKEY|ControlMask,    KEY,      ntoggleview,    {.ui = 1 << TAG} }, \
  { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
  { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *surfcmd[]  = { "surf", NULL };
static const char *emacscmd[] = { "emacsclient", "-c", NULL };

void custom_quit(const Arg *arg) { 
  quit(NULL);
  system("pkill dwm-run");
}

void inc_alsa(const Arg *arg){
  system("amixer set Master 10%+");
}

void dec_alsa(const Arg *arg){
  system("amixer set Master 10%-");
}

void mute_alsa(const Arg *arg){
  system("amixer set toggle");
}

static Key keys[] = {
  /* modifier                     key        function        argument */
  { MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
  { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
  { MODKEY|ShiftMask,             XK_w,      spawn,          {.v = surfcmd } },
  { MODKEY|ShiftMask,             XK_e,      spawn,          {.v = emacscmd } },
  { MODKEY,                       XK_r,      togglebar,      {0} },
  { MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
  { MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
  { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
  { MODKEY,                       XK_u,      incnmaster,     {.i = -1 } },
  { MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
  { MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
  { MODKEY,                       XK_Return, zoom,           {0} },
  { MODKEY,                       XK_Tab,    view,           {0} },
  { MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
  { MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
  { MODKEY,                       XK_n,      setlayout,      {.v = &layouts[1]} },
  { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
  { MODKEY,                       XK_space,  setlayout,      {0} },
  { MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
  { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
  { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
  /* i don't use two monitors, so i replace these commands with audio commands. */
  /* { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } }, */
  /* { MODKEY,                       XK_period, focusmon,       {.i = +1 } }, */
  /* { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } }, */
  /* { MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },	 */
  { MODKEY|ShiftMask,             XK_comma,  dec_alsa,       {0}},
  { MODKEY|ShiftMask,             XK_period, inc_alsa,       {0}},
  { MODKEY|ShiftMask,             XK_slash,  mute_alsa,      {0}},
  TAGKEYS(                        XK_1,                      0)
  TAGKEYS(                        XK_2,                      1)
  TAGKEYS(                        XK_3,                      2)
  TAGKEYS(                        XK_4,                      3)
  TAGKEYS(                        XK_5,                      4)
  TAGKEYS(                        XK_6,                      5)
  TAGKEYS(                        XK_7,                      6)
  TAGKEYS(                        XK_8,                      7)
  TAGKEYS(                        XK_9,                      8)
  /* { MODKEY,                       XK_grave,  reset_view,            {0} }, */
  { MODKEY|ShiftMask,             XK_q,      custom_quit,           {0} },
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click                event mask      button          function        argument */
  { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
  { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
  { ClkMonNum,            0,              Button1,        focusmon,       {.i = +1 } },
  { ClkMonNum,            0,              Button2,        reset_view,     {0} },
  { ClkMonNum,            0,              Button3,        focusmon,       {.i = -1 } },
  { ClkWinTitle,          0,              Button2,        zoom,           {0} },
  { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
  { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
  { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
  { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
  { ClkTagBar,            0,              Button1,        view,           {0} },
  { ClkTagBar,            0,              Button3,        toggleview,     {0} },
  { ClkTagBar,            MODKEY|WINKEY,  Button1,        nview,          {0} },
  { ClkTagBar,            MODKEY|WINKEY,  Button3,        ntoggleview,    {0} },
  { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
  { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
